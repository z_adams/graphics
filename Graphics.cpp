//Using SDL and standard IO
#include <SDL.h>
#include <stdio.h>
#include <Radar.h>
#include <Initializer.h>
#include <ObjectManager.h>
#include <thread>
#include "SurfaceBitmap.h"
//#include <cstdint>

//Screen dimension constants
const int SCREEN_WIDTH = 480;
const int SCREEN_HEIGHT = 480;

// Starts SDL and creates window
bool init();

// Loads media
bool loadMedia();

// Frees media and shuts down SDL
void close();

// The window we'll be rendering to
SDL_Window *gWindow = NULL;

// The surface contained by the window
SDL_Surface *gScreenSurface = NULL;

// The image we will load and show on the screen
SDL_Surface *gHelloWorld = NULL;

// Radar stuff...
RadarFunctions rd;

// Framebuffer
std::shared_ptr<SurfaceBitmap> fbuf;

enum Direction { Left, Right };

constexpr float FOVS[] = {120.0f, 60.0f, 30.0f};
int fovIndex = 0;
float azimuth = 0.0f;

Uint32 get_pixel(SDL_Surface *surface, int x, int y);
void set_pixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
void polar_to_cartesian(float r, float theta, float *x, float *y);
void radar_wipe(SDL_Surface *output, SDL_Surface *initSurf,
    SDL_Surface *endSurf, Direction sweepDirection, float position);
void make_checkers(SDL_Surface *surface, Uint32 color1, Uint32 color2);
void do_scan(SDL_Surface *next);

int main(int argc, char *args[])
{
    init();
    bool quit = false;
    bool readyToScan = true;
    float position = 0.0f;
    float yPos = -10.0f;
    Direction dir = Direction::Right;
    int scanPeriod = 2000;
    std::thread traceThread;
    SDL_Event e;
    SDL_Surface *frame = SDL_CreateRGBSurface(0, gScreenSurface->w,
        gScreenSurface->h, gScreenSurface->format->BitsPerPixel,
        gScreenSurface->format->Rmask, gScreenSurface->format->Gmask,
        gScreenSurface->format->Bmask, 0);
    SDL_Surface *a = SDL_CreateRGBSurface(0, gScreenSurface->w,
        gScreenSurface->h, gScreenSurface->format->BitsPerPixel,
        gScreenSurface->format->Rmask, gScreenSurface->format->Gmask,
        gScreenSurface->format->Bmask, 0);
    SDL_Surface *b = SDL_CreateRGBSurface(0, gScreenSurface->w,
        gScreenSurface->h, gScreenSurface->format->BitsPerPixel,
        gScreenSurface->format->Rmask, gScreenSurface->format->Gmask,
        gScreenSurface->format->Bmask, 0);
    SDL_Surface *black = SDL_CreateRGBSurface(0, gScreenSurface->w,
        gScreenSurface->h, gScreenSurface->format->BitsPerPixel,
        gScreenSurface->format->Rmask, gScreenSurface->format->Gmask,
        gScreenSurface->format->Bmask, 0);

    SDL_Surface *next = a, *prev = b;
    make_checkers(black, 0xFF000000, 0xFF000000);

    // ######### MAIN LOOP ##########
    while (!quit)
    {
        while (SDL_PollEvent(&e) != 0)
        {
            // User requests quit
            if (e.type == SDL_QUIT)
            {
                quit = true;
                traceThread.join();
            }
            if (e.type == SDL_KEYDOWN)
            {
                switch (e.key.keysym.sym)
                {
                    case SDLK_UP:
                        if (fovIndex > 0)
                            fovIndex--;
                        std::cout << "FoV: " << FOVS[fovIndex] << "\n";
                        break;
                    case SDLK_DOWN:
                        if (fovIndex < 2)
                            fovIndex++;
                        std::cout << "FoV: " << FOVS[fovIndex] << "\n";
                        break;
                    case SDLK_LEFT:
                        if (azimuth > -60.0f)
                            azimuth -= 10.0f;
                        std::cout << "Azm: " << azimuth << "\n";
                        break;
                    case SDLK_RIGHT:
                        if (azimuth < 60.0f)
                            azimuth += 10.0f;
                        std::cout << "Azm: " << azimuth << "\n";
                        break;
                    default:
                        break;
                }
            }
        }
        if (readyToScan)
        {
            rd.updatePosition(Vec3D_t(0.0f, yPos, 3.0f));
            rd.setAntennaAzimuth(azimuth);
            rd.setSweepAngle(FOVS[fovIndex]);
            traceThread = std::thread(do_scan, next);
            readyToScan = false;
        }
        float old_pos = position;
        //position = (float)(SDL_GetTicks() % scanPeriod) / scanPeriod;
        position += 0.004;
        yPos += 0.01;
        if (position > 1.0f)
        {
            position = 0.0f;
        }
        if (position - old_pos < 0)
        {
            traceThread.join();
            dir = (dir == Direction::Right) ? Direction::Left : Direction::Right;
            SDL_Surface *intermediate = prev;
            prev = next;
            next = intermediate;
            SDL_BlitSurface(fbuf->getSurface(), NULL, next, NULL);
            readyToScan = true;
        }
        radar_wipe(frame, prev, next, dir, position);

        //Apply image
        SDL_BlitSurface(frame, NULL, gScreenSurface, NULL);
        SDL_UpdateWindowSurface(gWindow);
    }
    close();
    return 0;
}

bool init()
{
    bool success = true;

    // Init SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        printf("SDL could not be initialized: %s\n", SDL_GetError());
    } else
    {
        // Create window
        gWindow = SDL_CreateWindow("Hacked government defense program",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH,
            SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (gWindow == NULL)
        {
            printf("Windows was not created: %s\n", SDL_GetError());
            success = false;
        }
        else
        {
            // Get window surface
            gScreenSurface = SDL_GetWindowSurface(gWindow);
        }
    }
    fbuf = std::shared_ptr<SurfaceBitmap>(
        new SurfaceBitmap(gScreenSurface->w, gScreenSurface->h,
        gScreenSurface->format->BitsPerPixel,
        gScreenSurface->format->Rmask, gScreenSurface->format->Gmask,
        gScreenSurface->format->Bmask, gScreenSurface->format->Amask));
    Initializer init("", fbuf);
    init.sceneObjects.processMesh("terrainDEM.obj", "terrainDEM.bmp");
    init.sceneObjects.createObject(0, Vec3D_t(0.0f, 0.0f, 0.0f));
    int numTris = 0;
    rd = init.build(&numTris);
    rd.setScanRange(40.0f);
    return success;
}

bool loadMedia()
{
    // loading success flag
    bool success = true;

    // Load splash image
    gHelloWorld = SDL_LoadBMP("rasterizedScope.bmp");
    if (gHelloWorld == NULL)
    {
        printf("Unable to load image: %s\n", SDL_GetError());
        success = false;
    }
    return success;
}

void close()
{
    // Deallocate surface
    SDL_FreeSurface(gHelloWorld);
    gHelloWorld = NULL;

    // Destroy window
    SDL_DestroyWindow(gWindow);
    gWindow = NULL;

    // Quit SDL
    SDL_Quit();
}

Uint32 get_pixel(SDL_Surface *surface, int x, int y)
{
    int bytePP = surface->format->BitsPerPixel / 8;
    if (bytePP == 3)
    {
        Uint8 *pixels = (Uint8*)surface->pixels;
        int pxPerRow = surface->pitch / bytePP;
        Uint32 *p = (Uint32*)(pixels + (y * pxPerRow + x) * bytePP);
        Uint32 val = (*p) & 0x00FFFFFF;
        val |= 0xFF000000;
        return val;
    } else
    {
        Uint32 *pixels = (Uint32*)surface->pixels;
        return pixels[y * surface->w + x];
    }
}

void set_pixel(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    Uint32 *pixels = (Uint32 *)surface->pixels;
    //int pitch = surface->pitch; // / sizeof(Uint32);
    pixels[y * surface->w + x] = pixel;
}

// Theta in degrees
void polar_to_cartesian(float r, float theta, float *x, float *y)
{
    theta *= M_PI / 180.;
    *x = r * cos(theta);
    *y = r * sin(theta);
}

void cartesian_to_polar(float x, float y, float *r, float *theta)
{
    *r = sqrt(x * x + y * y);
    *theta = atan2(y, x);
    *theta *= 180. / M_PI;
}

/* Position goes from 0 to 1.0f
*/
void radar_wipe(SDL_Surface *output, SDL_Surface *initSurf, 
    SDL_Surface *endSurf, Direction sweepDirection, float position)
{
    int width = initSurf->w;
    int height = initSurf->h;
    int direction = (sweepDirection == Direction::Right) ? 1 : -1;

    float fov = 120.0f;
    int x_mid = width / 2;
    float initAngle = 90.0f + (fov / 2.0f) * direction;
    float finalAngle = 90.0f - (fov / 2.0f) * direction;

    // Linearly interpolate
    float currentAngle = initAngle - fov * position * direction;

    if (SDL_MUSTLOCK(initSurf))
    {
        SDL_LockSurface(initSurf);
    }
    if (SDL_MUSTLOCK(endSurf))
    {
        SDL_LockSurface(endSurf);
    }
    for (int x = 0; x < width; x++)
    {
        int x_shifted = x - x_mid;  // Shift origin to center of screen
        for (int y = 0; y < height; y++)
        {
            int y_shifted = height - y;
            float theta = atan2(y_shifted, x_shifted) * 180./M_PI;
            if (theta < 90.0f - (fov/2.0f) || theta > 90.0f + (fov/2.0f))
            {
                Uint32 px = 0xFF000000;
                set_pixel(output, x, y, px);
            } else if (direction == Direction::Right)
            {
                if (theta <= currentAngle)
                {
                    Uint32 endSurfPx = get_pixel(initSurf, x, y);
                    set_pixel(output, x, y, endSurfPx);
                } else
                {
                    Uint32 initSurfPx = get_pixel(endSurf, x, y);
                    set_pixel(output, x, y, initSurfPx);
                }
            } else
            {
                if (theta >= currentAngle)
                {
                    Uint32 endSurfPx = get_pixel(initSurf, x, y);
                    set_pixel(output, x, y, endSurfPx);
                } else
                {
                    Uint32 initSurfPx = get_pixel(endSurf, x, y);
                    set_pixel(output, x, y, initSurfPx);
                }
            }
        }
    }
}

void make_checkers(SDL_Surface *surface, Uint32 color1, Uint32 color2)
{
    int height = surface->h;
    int width = surface->w;
    int block_width = width / 10;

    for (int x = 0; x < width; x++)
    {
        for (int y = 0; y < height; y++)
        {
            bool on = ((x / block_width) % 2 == 0) && 
                ((y / block_width) % 2 == 0) || 
                ((x / block_width) % 2 == 1) && 
                ((y / block_width) % 2 == 1);
            if (on)
            {
                set_pixel(surface, x, y, color1);
            } else
            {
                set_pixel(surface, x, y, color2);
            }
        }
    }
}

void do_scan(SDL_Surface *next)
{
    rd.clear();
    rd.sweep();
    rd.rasterize();
    return;
}
