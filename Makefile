objs := Graphics.o SurfaceBitmap.o
programs := Graphics.x

RTLIB := RT
RTPATH := $(RTLIB)
librt := $(RTPATH)/lib/$(RTLIB).a

CC := g++

all: $(objs) $(programs)

MAKEFLAGS += -rR

ifneq ($(D),1)
CFLAGS += -O2
else
CFLAGS += -O0
CFLAGS += -g
endif

LDFLAGS := -lSDL2 -lpthread
INCLUDE := -I $(RTPATH)/include

deps := $(patsubst %.o,%.d,$(objs))
-include $(deps)
DEPFLAGS = -MMD -MF $(@:.o=.d)

%.x: %.o
	$(CC) $(CFLAGS) -o $@ $(objs) $(librt) $(LDFLAGS)

%.o: %.cpp
	$(CC) $(CFLAGS) $(INCLUDE) -c -o $@ $< $(DEPFLAGS)

clean:
	rm -rf $(objs) $(deps) $(programs)
