#pragma once

#ifndef CAMERA_H
#define CAMERA_H

#include "Image.h"
#include "Matrix.h"
#include "RadarScope.h"
#include "ObjectManager.h"

class Camera
{
public:
    Vec3D_t origin, direction;
    Matrix4x4 cameraMatrix;
    BmpImage img;
    int xResolution, yResolution;
    float imgPlaneDepth, aspectRatio;
    int colorBitDepth;
    int sceneObjectCount;
    int sceneObjectsCapacity;
    std::shared_ptr<ObjectManager> sceneObjects;

    float calculateReflectivity(const Vec3D_t &hitNormal, const Vec3D_t &rayDirection, float impedance, float roughness) const; // debug

    void computeMatrix(float rX, float rY, float rZ);
    Pixel castRay(const Vec3D_t &direction);
    void reallocateSceneObjects(int newSize);
    void condenseSceneObjects();
    

    Camera(
        Vec3D_t origin, Vec3D_t direction, int xResolution, int yResolution,
        float foV, int colorBitDepth, float aspectRatio = 0.0f);
    Camera();
    ~Camera();
    Vec3D_t cameraToWorldCoords(const Vec3D_t &vector);
    void addObject(Mesh *mesh);
    void trace();
    void print(std::string outputFilepath) 
    { img.writeToFile(outputFilepath); }
};

#endif