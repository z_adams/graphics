#pragma once

#ifndef MESH_H
#define MESH_H

#include "Triangle.h"
#include "Matrix.h"
#include "Image.h"

struct SurfaceProperties;

class Mesh
{
public:
    std::shared_ptr<Face[]> faces;
    std::shared_ptr<Vec3D_t[]> vertices, normals;
    std::shared_ptr<Vec2D_t[]> uvs;
    int nVerts, nFaces, nNormals, nUVs;
public:
    std::shared_ptr<BmpImage> texture;

    // Create a mesh defined by the specified lists of geometry data
    Mesh(
        std::shared_ptr<Face[]> faceArray,
        std::shared_ptr<Vec3D_t[]> vertexArray,
        std::shared_ptr<Vec3D_t[]> normArray,
        std::shared_ptr<Vec2D_t[]> uvArray,
        int nVerts, int nFaces, int nNormals, 
        int nUVs, std::string textureFilepath);

    // Duplicate another mesh (shallow)
    Mesh(const Mesh &other);

    // Perform a shallow copy of the mesh data
    Mesh& operator=(const Mesh &other);
    Mesh() : 
        faces(nullptr), vertices(nullptr), normals(nullptr), 
        uvs(nullptr), nVerts(0), nFaces(0), nNormals(0), 
        nUVs(0), texture() {}
    SurfaceProperties getSurfaceProperties(
        const Vec3D_t &hitpoint,
        const Vec3D_t &viewDirection,
        const Face &face,
        const Vec2D_t &uv) const;
    void bounds(Vec3D_t *origin, float *spanX, float *spanY, float *spanZ);
};

struct SurfaceProperties
{
    Vec3D_t hitNormal;
    Vec2D_t texCoordinates;
};

struct Rotation
{
    Rotation(float rX, float rY, float rZ) : rX(rX), rY(rY), rZ(rZ) {}
    float rX;
    float rY;
    float rZ;
};

class SceneObject
{
private:
    Vec3D_t pos;
    Matrix4x4 rot;
    bool visible;
    int meshID;

    friend class ObjectManager;
public:
    Mesh *mesh;

    SceneObject(Vec3D_t position, Matrix4x4 rotation, Mesh* mesh = nullptr)
        : pos(position), rot(rotation), visible(true), mesh(mesh), meshID(0) {}
    SceneObject(Vec3D_t position, Rotation rotation, Mesh* mesh = nullptr)
        : pos(position), visible(true), mesh(mesh), meshID(0)
    {
        setRotation(rotation);
    }
    SceneObject(Vec3D_t position, Rotation rotation, int meshID)
        : pos(position), visible(true), meshID(meshID), mesh(nullptr)
    {
        setRotation(rotation);
    }
    SceneObject();
    void setPosition(Vec3D_t position) { pos = position; }
    void setRotation(float rX, float rY, float rZ)
    {
        rot = Matrix4x4::computeRotationMatrix(rX, rY, rZ);
    }
    void setRotation(Rotation rotation)
    {
        rot = Matrix4x4::computeRotationMatrix(
            rotation.rX, rotation.rY, rotation.rZ);
    }
    Vec3D_t getPosition() { return pos; }
    Matrix4x4 getRotation() { return rot; }
    bool isVisible() { return visible; }
    bool setVisibility(bool state) { visible = state; }
};
#endif
