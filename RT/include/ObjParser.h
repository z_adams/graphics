#pragma once

#ifndef OBJPARSER_H
#define OBJPARSER_H

#include <fstream>
#include "Triangle.h"
#include "Mesh.h"
#include <vector>

class ObjectManager;

class ObjParser
{
private:
    std::shared_ptr<Mesh> currentMesh;
    std::shared_ptr<Vec3D_t[]> vertices, vertNormals;
    std::shared_ptr<Vec2D_t[]> uvs;
    std::shared_ptr<Face[]> faces;
    int numVerts, numFaces, numNormals, numUVs;
    // Counts the number of each element in the file
    void count(std::ifstream &fs);
    // Parses the data from the file
    void parse(std::ifstream &fs);
    std::vector<std::shared_ptr<Mesh>> meshes;
    std::vector<SceneObject> objects;
    int numMeshes;
    int numObjects;

    friend class ObjectManager;
public:
    ObjParser() : numMeshes(0), numObjects(0), numVerts(0), numFaces(0),
        numNormals(0), numUVs(0) {}
    // Read an .obj file into memory
    void processMesh(std::string meshFilepath, std::string textureFilepath);

    // Spawn an instance of a mesh in the form of a unique object
    void createObject(int meshID, Vec3D_t position = Vec3D_t(0, 0, 0),
        Rotation rotation = Rotation(0, 0, 0));
};

#endif