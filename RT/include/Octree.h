#pragma once

#ifndef ZOCTREE_H
#define ZOCTREE_H

#include <memory>
#include <vector>
#include "Ray.h"
#include "Triangle.h"


/* class Box represents a geometric box primitive
 * Designed to be used in an octree bounding volume hierarchy
*/
class Box
{
protected:
    Vec3D_t position;
    float height, xWidth, yWidth;
public:
    Box(Vec3D_t position, float height, float xWidth, float yWidth) 
        : position(position), height(height), xWidth(xWidth), yWidth(yWidth) {}
    Vec3D_t getPosition() const { return position; }
    bool contains(Vec3D_t point) const;
    float zMaxBound() const { return position.z + (height / 2); }
    float zMinBound() const { return position.z - (height / 2); }
    float xMaxBound() const { return position.x + (xWidth / 2); }
    float xMinBound() const { return position.x - (xWidth / 2); }
    float yMaxBound() const { return position.y + (yWidth / 2); }
    float yMinBound() const { return position.y - (yWidth / 2); }
    bool intersectedBy(const Ray, float t0, float t1) const;
};

// HMM: T should probably be the raw type and T* the actual stored data
/*  A Volume is a member of an octree that stores either data (if the volume is
    a "leaf"), or child Volumes. Each Volume can store up to 8 children, the 
    octants of its volume. "Up to" because the octree is sparse.

    Octant naming standards are as follows:
    index   signature (x,y,z)
    0       (+ + +)
    1       (- + +)
    2       (- - +)
    3       (+ - +)
    4       (+ + -)
    5       (- + -)
    6       (- - -)
    7       (+ - -)
*/
template <typename T, int S>
class Volume : public Box
{
public:
    typedef typename std::vector<T>::const_iterator data_itr;
    // Data list, contains up to <S> elements
    std::vector<T> data;
    std::unique_ptr<Volume<T, S>> children[8];
    Volume<T, S> *parent;
    bool leaf;
public:
    Volume(Volume<T, S> *parent, Vec3D_t position, float height, float xWidth, 
        float yWidth) 
        : parent(parent), leaf(true), Box(position, height, xWidth, yWidth){}
    Volume() : parent(nullptr), leaf(true), Box(Vec3D_t(), 0, 0, 0) {}
    int octantIndex(Vec3D_t point) const;
    // Subdivide and redistrubute data into children
    bool subdivide();
    // Push data element into storage
    bool addData(T data);
    data_itr begin() const { return data.begin(); }
    data_itr end() const { return data.end(); }
    // Generate a child volume with the parameters of the specified octant
    std::unique_ptr<Volume<T, S>> generate(int octant);
};

/* class Zoctree stores a sparse octree of voxels Volume<T, S>
 * It behaves as a bounding volume hierarchy for ray-triangle intersection
 * search acceleration, either whole-object or sub-object.
 * A Volume<T, S> is an element of the octree which stores S of data T
 * Raycasting is performed into the octree and yields a buffer containing
 * any terminal ("leaf") nodes that were intersected. The data contained in
 * these nodes can then be linearly searched externally for an intersection.
 */
template <typename T, int S>
class Zoctree
{
private:
    typedef std::vector<Volume<T, S>*> vol_buffer;
    typedef typename vol_buffer::const_iterator buffer_itr;
    std::unique_ptr<Volume<T, S>> HEAD;
    vol_buffer searchBuffer;
    bool insert(T object, Volume<T, S> *current);
    void narrowIntersection(Ray ray, Volume<T, S> *current);
public:
    Zoctree(Vec3D_t position, float xWidth, float yWidth, float height)
        : HEAD(new Volume<T, S>(nullptr, position, height, xWidth, yWidth)) {}
    Zoctree() : HEAD(nullptr) {}
    bool insert(T object);
    void findHighestIntersectingVolume(Ray ray);
    int numResults() const { return searchBuffer.size(); }
    buffer_itr begin() const { return searchBuffer.begin(); }
    buffer_itr end() const { return searchBuffer.end(); }
    /*TODO: crazy highdea
        instead of providing a list of pointers at the end, spawn threads as 
        "results" are found, which go off and perform the ray-triangle 
        intersection for their box's contents before returning for another box.
        Eliminates ugly way of returning, more performant search.
    */
};

template<typename T, int S>
inline int Volume<T, S>::octantIndex(Vec3D_t point) const
{
    if (point.x > position.x)
    {
        if (point.y > position.y)
        {
            if (point.z > position.z)
                return 0;
            else
                return 4;
        } else
        {
            if (point.z > position.z)
                return 3;
            else
                return 7;
        }
    } else
    {
        if (point.y > position.y)
        {
            if (point.z > position.z)
                return 1;
            else
                return 5;
        } else
        {
            if (point.z > position.z)
                return 2;
            else
                return 6;
        }
    }
    return 0;
}

template<typename T, int S>
bool Volume<T, S>::subdivide()
{
    if (!leaf)
        return false;
    int octant;
    for (data_itr itr = data.begin(); itr != data.end(); itr++)
    {
        octant = octantIndex((*itr)->centroid());
        if (!children[octant])
        {
            children[octant] = generate(octant);
        }
        children[octant]->addData(*itr);
    }
    this->leaf = false;
    return true;
}

template<typename T, int S>
inline bool Volume<T, S>::addData(T data)
{
    if (this->data.size() >= S) 
    {
        return false;
    }
    this->data.push_back(data);
    return true;
}

template<typename T, int S>
inline std::unique_ptr<Volume<T, S>> Volume<T, S>::generate(int octant)
{
    float tHeight = this->height / 2;
    float tXWidth = this->xWidth / 2;
    float tYWidth = this->yWidth / 2;
    float x, y, z;
    if (octant == 0 || octant == 3 || octant == 4 || octant == 7)
    {
        x = this->position.x + (tXWidth / 2);
    } else
    {
        x = this->position.x - (tXWidth / 2);
    }
    if (octant == 0 || octant == 1 || octant == 4 || octant == 5)
    {
        y = this->position.y + (tYWidth / 2);
    } else
    {
        y = this->position.y - (tYWidth / 2);
    }
    if (octant == 0 || octant == 1 || octant == 2 || octant == 3)
    {
        z = this->position.z + (tHeight / 2);
    } else
    {
        z = this->position.z - (tHeight / 2);
    }
    return std::unique_ptr<Volume<T, S>>(
            new Volume(this, Vec3D_t(x, y, z), tHeight, tXWidth, tYWidth));
}

template<typename T, int S>
bool Zoctree<T, S>::insert(T object)
{
    if (!HEAD->contains(object->centroid()))
    {
        return false;
    }
    return insert(object, HEAD.get());
}

template<typename T, int S>
bool Zoctree<T, S>::insert(T object, Volume<T, S>* current)
{
    if (current->leaf && current->data.size() < S)
    {
        current->data.push_back(object);
        return true;
    }
    if (current->leaf && current->data.size() >= S)
    {
        current->subdivide();
    }
    int octant = current->octantIndex(object->centroid());
    if (current->children[octant] == nullptr)
    {
        current->children[octant] = current->generate(octant);
    }
    return insert(object, current->children[octant].get());
}

template<typename T, int S>
void Zoctree<T, S>::findHighestIntersectingVolume(Ray ray)
{
    searchBuffer.clear();
    if (!HEAD->intersectedBy(ray, ray.tMin, ray.tMax))
    {
        return;
    }
    if (HEAD->leaf)
    {
        searchBuffer.push_back(HEAD.get());
        return;
    }
    narrowIntersection(ray, HEAD.get());
}

template<typename T, int S>
void Zoctree<T, S>::narrowIntersection(Ray ray, Volume<T, S>* parent)
{
    for (int i = 0; i < 8; i++)
    {
        if (!parent->children[i])
        {
            continue;
        }
        if (parent->children[i]->intersectedBy(ray, ray.tMin, ray.tMax))
        {
            if (parent->children[i]->leaf)
            {
                searchBuffer.push_back(parent->children[i].get());
            } else
            {
                narrowIntersection(ray, parent->children[i].get());
            }
        }
    }
}

#endif