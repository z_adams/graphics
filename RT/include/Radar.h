#pragma once

#ifndef RADAR_H
#define RADAR_H

#include "RadarScope.h"
#include "ObjectManager.h"

float rad(float degrees);

class RadarData
{
private:
    friend class RadarFunctions;

    /* Since the entire sweep is calculated in one pass, the width of the
     field of scan is described by this sweep angle
     */
    float sweepAngle;
    /* Azimuth and antennaAzmLimit describe the azimuth of the scan pattern.
     For example, if the aircraft is flying due north (bearing 000) with
     the radar in a narrow 30 degree sweep scanning between 320 and 350,
     the azimuth would be -25. The limit is the maximum angle the *antenna*
     can be slewed, not the whole pattern (since that value is dependent on
     scan width).
     */
    float azimuth, antennaAzmLimit;
    /* The elevation governs the maximum range of the ground radar returns,
     and the limits describe the limits of the antenna elevation. There is
     no elevation scan pattern in ground mapping mode.
     */
    float antennaElev, bottomElevLimit, topElevLimit;

    // Horizontal and vertical angular size of the beam, respectively
    float beamWidth, fanVertWidth;

    // Horizontal and vertical resolutions (unused)
    int horizResolution, rangeResolution;

    // Distance between impacts of rays, in nmi (sorry). 0.01 ~= 20m
    float pointResolution;

    // Boreline of the aircraft
    Vec3D_t boreline;

    // Position of the radar in space
    Vec3D_t position;
public:
    RadarData(Vec3D_t position, Vec3D_t boreline, float sweepAngle,
        float antennaAzmLimit, float bottomElevLimit, float topElevLimit,
        float beamWidth, float fanVertWidth, float rangeResolution);
};

class RadarFunctions
{
private:
    // Stores parameters of the vehicle and emitter
    std::unique_ptr<RadarData> data;

    // Responsible for processing raw data into display output
    std::unique_ptr<RadarScope> display;

    // Responsible for managing and probing scene geometry
    std::shared_ptr<ObjectManager> sceneObjects;

    Vec3D_t sphericalToCartesian(
        const float r, const float theta, const float phi);

    // Cast a single ray into the scene in the specified direction
    RadarReturn castDepthRay(const Vec3D_t &direction) const;
    // Perform a raytracing pass over the radar's field of view
    void radarTrace();
    // Calculate the magnitude of the energy reflected from the hit surface
    float calculateReflectivity(
        const Vec3D_t &hitNormal, const Vec3D_t &rayDirection, 
        float relDielectricConst, float roughness) const;
    static constexpr float EPSILON_0 = 8.854187e-12; // Vacuum permittivity
    static constexpr float Z_FREE_SPACE = 377; // Impedence free space, Ohms

public:
    RadarFunctions() : data(), display(), sceneObjects() {}
    RadarFunctions(
        std::unique_ptr<RadarData> radarData,
        std::unique_ptr<RadarScope> radarDisplay,
        std::shared_ptr<ObjectManager> sceneObjects)
        : data(std::move(radarData)), 
        display(std::move(radarDisplay)), 
        sceneObjects(std::move(sceneObjects)) {}
    // Update position of the radar emitter
    void updatePosition(Vec3D_t pos) { data->position = pos; }
    // Set the range of the radar scan
    bool setScanRange(float range);
    // Set the angular width of the scan
    bool setSweepAngle(float sweepAngle);
    // Set the azimuth of the center of the scan
    bool setAntennaAzimuth(float azimuth);
    // Steer the elevation of the antenna
    bool setAntennaElevation(float antennaElev);
    // Command the radar to perform a scan of its field of view
    void sweep() { radarTrace(); }
    // Rasterize the point cloud into the radar scope output image
    void rasterize() { display->mapImage(); }
    // Clear the previous sweep data
    void clear() { display->clear(); }
    // Write rasterized radar scope to a file
    void write(std::string outputFilepath) 
    { display->writeToFile(outputFilepath); }
    // Write raw point cloud to a file
    bool writeRaw(std::string outputFilepath) 
    { return display->writeRawToFile(outputFilepath); }
};

/* Terrain RCS sigma is given by
    sigma = sigma^0 * area
          = sigma^0 * (c*Tau*R*theta_az)/2cos(phi)
where sigma^0 is the backscatter coefficient, c is the speed of light,
tau is the pulse width, R is the range, theta_az is azimuthal beam width,
and phi is the look down angle

Clutter return = (P_t * G^2 * lambda^2)/((4pi)^3 * R^4) * A * sigma^0 watts

However, sigma^0 is a function of angle which needs to be accounted for
*/

struct BackscatterCoefficients
{

};

#endif
