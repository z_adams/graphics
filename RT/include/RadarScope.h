#pragma once

#ifndef RADARSCOPE_H
#define RADARSCOPE_H

#include "Image.h"
#include "Vec3.h"
#include "Mesh.h"

struct RadarReturn
{
    float power;
    float range;
    RadarReturn(float power, float range)
        : power(power), range(range) {}
    RadarReturn() : power(), range(0.0f) {}
};

struct AggregatePixel
{
    float value;
    float area;
    int nValues = 0;
    AggregatePixel(float area) : area(area), value(0.0f) {}
    AggregatePixel() : area(0.0f), value(0.0f) {}
};

/* TODO (successfully) implement a parent RadarDisplay object inherited by
 * various types of radar displays (A-scope, B-scope, C-scope, etc)
*/
class RadarDisplay
{
protected:
    float minRange, maxRange;
    std::unique_ptr<BmpImage> output;
    RadarDisplay() : minRange(0), maxRange(0), output(nullptr) {}
    RadarDisplay(std::unique_ptr<BmpImage> output) { this->output = std::move(output); }
    virtual ~RadarDisplay() = 0;
public:
    virtual void mapImage() = 0;
    virtual void changeRangeLimits(float min, float max) = 0;
    virtual float getMinRange() = 0;
    virtual float getMaxRange() = 0;
    virtual void mapPixel(float x, RadarReturn &retn) = 0;
    virtual void writeToFile(std::string filepath) = 0;
};

class RadarScope// : RadarDisplay
{
private:
    bool debugImgAvailable;
public:
    int rangeSegments, maxRangeSegments, pulsesPerSweep;
    float minRange, maxRange, cornerRange, rangeLimit;
    float rangeBinWidth, azimuthBinWidth;
    /*BmpImage output;
    BmpImage rasterizedOutput;*/
    std::shared_ptr<BitMap> output;
    std::shared_ptr<BitMap> rasterizedOutput;
    AggregatePixel& indexPolarPixel(float azimuth, float range);
    //RadarDisplay() : minRange(0), maxRange(0), output(nullptr) {}
    //RadarDisplay(std::unique_ptr<BmpImage> output) { this->output = std::move(output); }
public:
    // Collapse the dead region nearer than min range of beam coverage
    static constexpr bool COLLAPSE_MIN_RANGE = false;
    std::unique_ptr<AggregatePixel[]> pPArray;
    float foV, azimuthalBeamWidth;
    int azimuthRes;
    void screenToRasterSpace(Vec2D_t &vec);
    static float rad(float degrees);
    float computeCornerRange(float range);
    void computeCornerRange();

    // For testing the rasterizer on an existing image
    /*RadarScope(BmpImage &baseImage) : output(baseImage), 
        rasterizedOutput("../../radarScope.bmp", baseImage) {}*/
    RadarScope( 
        std::shared_ptr<BitMap> rasterizedOutput,int azimuthRes,
        float azimuthalBeamWidth, int rangeSegments, float rangeLimit,
        std::shared_ptr<BitMap> debugOutput = nullptr);
    ~RadarScope();
    void mapImage();
    // Change range limits and recompute display parameters
    void changeRangeLimits(float min, float max);
    // Change the angular width of the scan pattern
    bool setFoV(float foV);
    bool setAzimuthRes(int AztRes) { this->azimuthRes = AztRes; }
    float getMinRange() { return minRange; }
    float getMaxRange() { return maxRange; }
    Pixel fragShader(AggregatePixel bin);
    //void mapPixel(float x, RadarReturn &retn) /* inheritance */;
    void mapPixelSpherical(float theta, RadarReturn &retn);
    void mapPoint(float theta, RadarReturn &retn);
    void clear();
    bool debugOutputAvailable() const { return debugImgAvailable; }
    void writeToFile(std::string filepath)
    { rasterizedOutput->writeToFile(filepath); }
    bool writeRawToFile(std::string filepath)
    { 
        if (debugImgAvailable)
        {
            output->writeToFile(filepath);
            return true;
        }
        return false;
    }
};

#endif