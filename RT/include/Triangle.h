#pragma once

#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Ray.h"

struct Triangle
{
    Vec3D_t vert0;
    Vec3D_t vert1;
    Vec3D_t vert2;
    Vec3D_t normal() const
    {
        return ((vert1 - vert0).cross(vert2 - vert0)).normalize();
    }

    // C. Ericson "Real-Time Collision Detection"
    void Barycentric(Vec3D_t p, float &u, float &v, float &w) const
    {
        Barycentric(p, vert0, vert1, vert2, u, v, w);
    }

    inline static void Barycentric(Vec3D_t p, Vec3D_t vertex0, Vec3D_t vertex1, 
        Vec3D_t vertex2, float &u, float &v, float &w)
    {
        Vec3D_t v0 = vertex1 - vertex0, v1 = vertex2 - vertex0, v2 = p - vertex0;
        float d00 = v0 * v0;
        float d01 = v0 * v1;
        float d11 = v1 * v1;
        float d20 = v2 * v0;
        float d21 = v2 * v1;
        float denom = d00 * d11 - d01 * d01;
        v = (d11 * d20 - d01 * d21) / denom;
        w = (d00 * d21 - d01 * d20) / denom;
        u = 1.0f - v - w;
    }

    inline Vec3D_t centroid()
    {
        float x = (vert0.x + vert1.x + vert2.x) / 3;
        float y = (vert0.y + vert1.y + vert2.y) / 3;
        float z = (vert0.z + vert1.z + vert2.z) / 3;
        return Vec3D_t(x, y, z);
    }
};

struct Face
{
    Triangle tri;

    int normIndex0;
    int normIndex1;
    int normIndex2;

    int uvIndex0;
    int uvIndex1;
    int uvIndex2;

    void addVert(int i, Vec3D_t vertex, int uvIndex, int normIndex)
    {
        switch (i)
        {
            case 0:
                tri.vert0 = vertex;
                normIndex0 = normIndex;
                uvIndex0 = uvIndex;
            case 1:
                tri.vert1 = vertex;
                normIndex1 = normIndex;
                uvIndex1 = uvIndex;
            case 2:
                tri.vert2 = vertex;
                normIndex2 = normIndex;
                uvIndex2 = uvIndex;
            default:
                break;
        }
    }

    Triangle translate(const Vec3D_t &offset)
    {
        Triangle output;
        output.vert0 = tri.vert0 + offset;
        output.vert1 = tri.vert1 + offset;
        output.vert2 = tri.vert2 + offset;
        return output;
    }

    Vec3D_t centroid()
    {
        return tri.centroid();
    }

    float maxX()
    {
        float max = (tri.vert0.x > tri.vert1.x) ? tri.vert0.x : tri.vert1.x;
        if (tri.vert2.x > max)
            max = tri.vert2.x;
        return max;
    }

    float maxY()
    {
        float max = (tri.vert0.y > tri.vert1.y) ? tri.vert0.y : tri.vert1.y;
        if (tri.vert2.y > max)
            max = tri.vert2.y;
        return max;
    }

    float maxZ()
    {
        float max = (tri.vert0.z > tri.vert1.z) ? tri.vert0.z : tri.vert1.z;
        if (tri.vert2.z > max)
            max = tri.vert2.z;
        return max;
    }

    float minX()
    {
        float min = (tri.vert0.x < tri.vert1.x) ? tri.vert0.x : tri.vert1.x;
        if (tri.vert2.x < min)
            min = tri.vert2.x;
        return min;
    }

    float minY()
    {
        float min = (tri.vert0.y < tri.vert1.y) ? tri.vert0.y : tri.vert1.y;
        if (tri.vert2.y < min)
            min = tri.vert2.y;
        return min;
    }

    float minZ()
    {
        float min = (tri.vert0.z < tri.vert1.z) ? tri.vert0.z : tri.vert1.z;
        if (tri.vert2.z < min)
            min = tri.vert2.z;
        return min;
    }

    Vec3D_t maximumCoords()
    {
        return Vec3D_t(maxX(), maxY(), maxZ());
    }

    Vec3D_t minimumCoords()
    {
        return Vec3D_t(minX(), minY(), minZ());
    }
};

bool intersect(
    const Ray, const Triangle &inTri, Vec3D_t &intersectPoint, Vec2D_t &uv);

#endif