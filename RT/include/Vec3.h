#pragma once

#ifndef VECTOR3_H
#define VECTOR3_H

#define _USE_MATH_DEFINES
#include <cmath>

template<typename T>
class Vec2
{
public:
    T x, y;
    Vec2(T x, T y) : x(x), y(y) {}
    Vec2();

    void set(T x, T y) { this->x = x; this->y = y; }
    T dot(const Vec2 &other) const
    {
        return (this->x * other.x + this->y * other.y);
    }
    T norm() const;
    Vec2<T> normalize() const;
    const Vec2<T> operator+(const Vec2 &rhs) const;
    const Vec2<T> operator-(const Vec2 &rhs) const;
    const T operator*(const Vec2 &rhs) const;
    const Vec2<T> operator*(const T &rhs) const;
    Vec2<T>& operator*=(const T &rhs);
};

template<typename T>
class Vec3
{
public:
    T x, y, z;
    Vec3(T x, T y, T z) : x(x), y(y), z(z) {}
    Vec3() : Vec3((T)0.0f, (T)0.0f, (T)0.0f) {}

    void set(T x, T y, T z);
    T dot(const Vec3 &other) const
    {
        return this->x * other.x + this->y * other.y + this->z * other.z;
    }
    Vec3<T> cross(const Vec3 &other) const;
    T norm() const;
    Vec3<T> normalize() const;
    const Vec3<T> operator+(const Vec3 &rhs) const;
    const Vec3<T> operator-(const Vec3 &rhs) const;
    const T operator*(const Vec3 &rhs) const;
    const Vec3<T> operator*(const T &rhs) const;
    Vec3<T>& operator*=(const T &rhs);
    operator Vec2<T>() const { return Vec2<T>(x, y); }
};

template<typename T>
class Vec4 : public Vec3<T>
{
public:
    using Vec3<T>::x;
    using Vec3<T>::y;
    using Vec3<T>::z;
    T w;
    Vec4(T x, T y, T z, T w) : Vec3<T>(x, y, z), w(w) {}
    Vec4(const Vec3<T> &vector);
    Vec4();

    void set(T x, T y, T z, T w);
    T dot(const Vec4 &other) const
    {
        return (this->x * other.x + this->y * other.y +
            this->z * other.z + this->w * other.w);
    }
    //Vec4 cross(const Vec4 &other) const;
    T norm() const;
    Vec4 normalize() const;
    const Vec4<T> operator+(const Vec4 &rhs) const;
    const Vec4<T> operator-(const Vec4 &rhs) const;
    const Vec4<T> operator*(const T &rhs) const;
    Vec4<T>& operator*=(const T &rhs);
};

typedef Vec2<float> Vec2D_t;
typedef Vec3<float> Vec3D_t;
typedef Vec4<float> Vec4D_t;

template<typename T>
void Vec3<T>::set(T x, T y, T z)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

template<typename T>
Vec3<T> Vec3<T>::cross(const Vec3 & other) const
{
    T xComponent = (this->y * other.z - this->z * other.y);
    T yComponent = (this->z * other.x - this->x * other.z);
    T zComponent = (this->x * other.y - this->y * other.x);
    return Vec3(xComponent, yComponent, zComponent);
}

template<typename T>
T Vec3<T>::norm() const
{
    return T(sqrt(x * x + y * y + z * z));
}

template<typename T>
Vec3<T> Vec3<T>::normalize() const
{
    T norm = this->norm();
    return Vec3(this->x / norm, this->y / norm, this->z / norm);
}

template<typename T>
const Vec3<T> Vec3<T>::operator+(const Vec3 & rhs) const
{
    Vec3<T> result = (*this);
    result.x += rhs.x;
    result.y += rhs.y;
    result.z += rhs.z;
    return result;
}

template<typename T>
const Vec3<T> Vec3<T>::operator-(const Vec3 & rhs) const
{
    Vec3<T> result = (*this);
    result.x -= rhs.x;
    result.y -= rhs.y;
    result.z -= rhs.z;
    return result;
}

template<typename T>
const T Vec3<T>::operator*(const Vec3 & rhs) const
{
    return this->dot(rhs);
}

template<typename T>
const Vec3<T> Vec3<T>::operator*(const T & rhs) const
{
    return Vec3<T>(this->x * rhs, this->y * rhs, this->z * rhs);
}

template<typename T>
Vec3<T>& Vec3<T>::operator*=(const T & rhs)
{
    this->x *= rhs;
    this->y *= rhs;
    this->z *= rhs;
    return *this;
}

template<typename T>
Vec4<T>::Vec4(const Vec3<T>& vector)
{
    this->x = vector.x;
    this->y = vector.y;
    this->z = vector.z;
    this->w = T(1);
}

template<typename T>
Vec4<T>::Vec4()
{
    set(T(0), T(0), T(0), T(0));
}

template<typename T>
void Vec4<T>::set(T x, T y, T z, T w)
{
    this->x = T(0);
    this->y = T(0);
    this->z = T(0);
    this->w = T(0);
}

template<typename T>
T Vec4<T>::norm() const
{
    return sqrt(x*x + y * y + z * z + w * w);
}

template<typename T>
Vec4<T> Vec4<T>::normalize() const
{
    T norm = norm();
    return Vec4(x / norm, y / norm, z / norm, w / norm);
}

template<typename T>
const Vec4<T> Vec4<T>::operator+(const Vec4 & rhs) const
{
    Vec4<T> result;
    result.x = x + rhs.x;
    result.y = y + rhs.y;
    result.z = z + rhs.z;
    result.w = w + rhs.w;
    return result;
}

template<typename T>
const Vec4<T> Vec4<T>::operator-(const Vec4 & rhs) const
{
    Vec4<T> result;
    result.x = x - rhs.x;
    result.y = y - rhs.y;
    result.z = z - rhs.z;
    result.w = w - rhs.w;
    return result;
}

template<typename T>
const Vec4<T> Vec4<T>::operator*(const T & rhs) const
{
    return Vec4<T>(x * rhs, y * rhs, z * rhs, w * rhs);
}

template<typename T>
Vec4<T>& Vec4<T>::operator*=(const T & rhs)
{
    x *= rhs;
    y *= rhs;
    z *= rhs;
    w *= rhs;
    return *this;
}

template<typename T>
Vec2<T>::Vec2()
{
    set(T(0), T(0));
}

template<typename T>
T Vec2<T>::norm() const
{
    return T(sqrt(x * x + y * y));
}

template<typename T>
Vec2<T> Vec2<T>::normalize() const
{
    T norm = this->norm();
    return Vec2(this->x / norm, this->y / norm);
}

template<typename T>
const Vec2<T> Vec2<T>::operator+(const Vec2 &rhs) const
{
    return Vec2<T>(this->x + rhs.x, this->y + rhs.y);
}

template<typename T>
const Vec2<T> Vec2<T>::operator-(const Vec2 &rhs) const
{
    return Vec2<T>(this->x - rhs.x, this->y - rhs.y);
}

template<typename T>
const T Vec2<T>::operator*(const Vec2 &rhs) const
{
    return this->dot(rhs);
}

template<typename T>
const Vec2<T> Vec2<T>::operator*(const T &rhs) const
{
    return Vec2<T>(this->x * rhs, this->y * rhs);
}

template<typename T>
Vec2<T>& Vec2<T>::operator*=(const T & rhs)
{
    this->x *= rhs;
    this->y *= rhs;
    return *this;
}

#endif