#include "SurfaceBitmap.h"

SurfaceBitmap::SurfaceBitmap(int width, int height, int bitsPerPx, 
    Uint32 Rmask, Uint32 Gmask, Uint32 Bmask, Uint32 Amask)
    : surface(nullptr)
{
    surface = SDL_CreateRGBSurface(0, width, height, bitsPerPx, 
        Rmask, Gmask, Bmask, Amask);
}

bool SurfaceBitmap::setPixel(int x, int y, Pixel px)
{
    if (x > surface->w || y > surface->h || x < 0 || y < 0)
        return false;
    Uint32 *pixels = (Uint32 *)surface->pixels;
    Uint32 red = px.r << surface->format->Rshift;
    Uint32 green = px.g << surface->format->Gshift;
    Uint32 blue = px.b << surface->format->Bshift;
    Uint32 alpha = 0xFF << surface->format->Ashift;
    Uint32 pixel = 0x0 | (px.r << 16) | (px.g << 8) | (px.b);
    pixels[(surface->h - y) * surface->w + x] = pixel;
    return true;
}

Pixel SurfaceBitmap::getPixel(int x, int y) const
{
    Uint32 *pixels = (Uint32 *)surface->pixels;
    Uint32 pixel = pixels[y * surface->w + x];
    Pixel px;
    px.r = (pixel & surface->format->Rmask) >> surface->format->Rshift;
    px.g = (pixel & surface->format->Gmask) >> surface->format->Gshift;
    px.b = (pixel & surface->format->Bmask) >> surface->format->Bshift;
    return px;
}

Uint32 SurfaceBitmap::getPixel32(int x, int y) const
{
    int bytePP = surface->format->BitsPerPixel / 8;
    if (bytePP == 3)
    {
        Uint8 *pixels = (Uint8 *)surface->pixels;
        int pxPerRow = surface->pitch / bytePP;
        Uint32 *p = (Uint32 *)(pixels + (y * pxPerRow + x) * bytePP);
        Uint32 val = (*p) & 0x00FFFFFF;
        val |= 0xFF000000;
        return val;
    } else
    {
        Uint32 *pixels = (Uint32 *)surface->pixels;
        return pixels[y * surface->w + x];
    }
}

int SurfaceBitmap::bitsPerPx() const
{
    return surface->format->BitsPerPixel;
}

int SurfaceBitmap::xWidth() const
{
    return surface->w;
}

int SurfaceBitmap::yHeight() const
{
    return surface->h;
}

void SurfaceBitmap::writeToFile(std::string filepath)
{
    return;
}

SDL_Surface *SurfaceBitmap::getSurface()
{
    return surface;
}
