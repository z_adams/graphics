#pragma once

#ifndef G_SURF_BMP_H
#define G_SURF_BMP_H

#include <Image.h>
#include <SDL.h>

class SurfaceBitmap : public BitMap
{
private:
    SDL_Surface *surface;
public:
    SurfaceBitmap(int width, int height, int bitsPerPx, Uint32 Rmask,
        Uint32 Gmask, Uint32 Bmask, Uint32 Amask);
    SurfaceBitmap() : surface(nullptr) {}
    
    bool setPixel(int x, int y, Pixel px);
    bool setPixel(int x, int y, Uint32 px);
    Pixel getPixel(int x, int y) const;
    Uint32 getPixel32(int x, int y) const;
    int bitsPerPx() const;
    int xWidth() const;
    int yHeight() const;
    void writeToFile(std::string filepath);
    SDL_Surface *getSurface();
};


#endif